import { Component,OnInit,OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit,OnDestroy{
  private test;

  title = 'UI';

  constructor(){
    console.log("hi")
    this.test = "test0";
  }
  ngOnInit(){
    this.test = "test1";
  }
  ngOnDestroy(){
    console.log("finally")
  }
}
