import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators, FormControl} from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  private registerForm:FormGroup ;
  usertypes = [
    {name: 'user-1'},
    {name: 'user-2'},
    {name: 'user-3'}
  ];
  constructor(private formBuilder:FormBuilder,) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username:["",Validators.required],
      password:["",Validators.required],
    //  confirmPassword: ["",Validators.required],
      email:new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      userType:["",Validators.required]
    })
  }

  private register(){
    if(this.registerForm.valid){
      console.log(this.registerForm.getRawValue());
    }else{
      console.log("error");
    }
  }

  
}
