import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators} from '@angular/forms';
import {Router} from '@angular/router'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
 private loginForm:FormGroup ;
  constructor(private formBuilder:FormBuilder,private _router:Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username:["",Validators.required],
      password:["",Validators.required]
    })
  }

  private login(){
    if(this.loginForm.valid){
      console.log(this.loginForm.getRawValue());
    }else{
      console.log("error");
    }
   
  }
  private register(){
    this._router.navigateByUrl("register");
  }

}
